{{ download_stats}}

Welcome to the documentation for django-{{ download_stats }}!
Running the Tests

You can run the tests with via:

python setup.py test

or:

python runtests.py


