from django.conf.urls import patterns, include
from django.contrib import admin
import download_stats.urls

admin.autodiscover()

admin.autodiscover()

urlpatterns = patterns('',
    (r'^admin/', include(admin.site.urls)),
    (r'^download/', include(download_stats.urls)),
)
