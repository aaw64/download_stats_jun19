"""

Dada
This file demonstrates writing tests using the unittest module. These will pass
when you run "manage.py test".

Replace this with more appropriate tests for your application.
"""
"""
from django.test import TestCase
from django.utils import timezone
from models import DownloadFile, DownloadRecord
from download_stats.helpers import FileRegistry , FileWrapper
from django.http import HttpRequest , HttpResponse , Http404
from django.core.urlresolvers import reverse
from django.test.client import Client 
from django.conf import settings
import mock
import os
import tempfile
import datetime


class Download_StatsTest(TestCase):
    def create_tempfile(self,contents=''):
        f = tempfile.NamedTemporaryFile(delete=False)
        disk_path = f.name
        f.write(contents)
        f.close()
        return disk_path  
 
    def setUp(self):
        settings.SENDFILE_BACKEND = 'sendfile.backends.simple'
        self.url = '/download/temp.file'
        self.path = self.create_tempfile('this is a test')

        DownloadFile.objects.create(
                path = self.path,
                url = self.url
        )

    def tearDown(self):
        os.unlink(self.path)

    def test_valid(self):
        url = reverse('download_file', kwargs={'filename': 'temp.file'})
        response = self.client.get(url)
        content = response.content

        self.assertIsInstance(response,HttpREsponse)
        self.assertEqual(response.status_code,200)
        self.assertEqual(content,'this is a test')
"""






        
 




   


     

